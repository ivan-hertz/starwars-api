import React, { Component } from 'react';
import { connect } from 'react-redux';
import { IContent, ISpecies } from './common/interfaces';
import { SearchBox } from './components/SearchBox';
import { SearchResults } from './components/SearchResults';
import { Species } from './components/Species';
import { Loading } from './components/Loading';
import { ContentDetails } from './components/ContentDetails';
import { SearchService } from './services/SearchService';
import { SpeciesService } from './services/SpeciesService';
import { contentInitialState } from './store/content/content.reducer';
import { IRootReducers } from './store/store.reducer';
import { clearSearchResults } from './store/search/search.actions';
import Store from './store/store';

import './App.css';

interface IComponentProps {
  loading: boolean;
  content: IContent;
  search: IContent[] | null;
  species: ISpecies[];
}

/**
 * This main smart container, 
 * using mapStateToProps 
 * for performance purpose.
 */

class App extends Component<IComponentProps> {
  public constructor(props: IComponentProps) {
    super(props);
    this._executeSearch = this._executeSearch.bind(this);
    this.getHome = this.getHome.bind(this);
  }

  // App is the Homepage
  componentDidMount() {
    SpeciesService.getAllSpecies();
  }
  public static defaultProps: IComponentProps = {
    loading: false,
    content: contentInitialState,
    search: null,
    species: [],
  };

  /**
   * It felt "cracking a nut with the Large Hadron Collider"
   * to put the SearchService into the SearchBox component.
   * It much more reasonable on the App scope.
   *
   * @param text - input value from validated state
   */
  public _executeSearch(text: string) {
    SearchService.searchByKeyword(text);
  }

  /**
   * Navigates to home, resets data
   */
  private getHome() {
    Store.dispatch(clearSearchResults());
    SpeciesService.getAllSpecies();
  }

  public render(): React.ReactElement {
    return (
      <div className="App">
        <Loading showLoader={this.props.loading} />
        <header className="App-header">
          <div className="App-logo"></div>
          <div className="home" onClick={this.getHome}>
            Home
          </div>
          <SearchBox executeSearch={this._executeSearch} />
        </header>
        {this.props.search !== null && <SearchResults />}
        {this.props.search === null && <Species />}
        {this.props.content?.name && <ContentDetails />}
      </div>
    );
  }
}

/**
 * The only place for controlling the single page
 * with performance, no useState
 */
const mapStateToProps = (state: IRootReducers) => ({
  loading: state.application.loading,
  content: state.content,
  search: state.search.results,
  species: state.species.results,
});

export default connect(mapStateToProps, null)(App);
