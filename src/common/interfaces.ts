
export interface IContent {
    birth_year: string;
    created: string;
    edited: string;
    eye_color: string;
    films: Array<string>;
    gender: string;
    hair_color: string;
    height: number | null;
    homeworld: string;
    mass: number | null;
    name: string;
    skin_color: string;
    species: Array<string>;
    starships: Array<string>;
    url: string;
    vehicles: Array<string>;
}
export interface IResponse {
    count?: number | null;
    next?: any | null;
    previous?: any | null;
    results?: any | null;
}

export interface ISpecies {
    average_height: string;
    average_lifespan: string;
    classification: string;
    created: string;
    designation: string;
    edited: string;
    eye_colors: string;
    films: Array<string>,
    hair_colors: string;
    homeworld: string;
    language: string;
    name: string;
    people: Array<string>;
    skin_colors: string;
    url: string;
}