import styled from 'styled-components';

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  position: fixed;
  z-index: 9999;
  top: 0;
  background-color: rgba(0, 0, 0, 0.7);
  color: #fff;
  width: 100%;
  margin: 0 auto;
  height: 100%;
  text-align: left;
  h2 {
    color: #fff !important;
  }
  div {
    max-width: 600px;
    min-width: 400px;
    background-color: #424242 !important;
  }
  .card {
    max-width: 345px;
    width: 300px;
  }
`;

export default Wrapper;
