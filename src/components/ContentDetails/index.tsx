import { useSelector } from 'react-redux';
import { IContent } from '../../common/interfaces';
import { clearContent } from '../../store/content/content.actions';
import { selectContent } from '../../store/content/content.selectors';
import Store from '../../store/store';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grow from '@material-ui/core/Grow';
import Wrapper from './Wrapper';

export function ContentDetails() {
  const content: IContent = useSelector(selectContent);
  const closeContentDetailsPanel = () => Store.dispatch(clearContent());
  /**
   * Renders the whole Details modal nicely by selecting the content state
   * Note: Species property disabled here, due to the exact same
   * feature of the 'films' prop.
   */
  return (
    <Wrapper>
      <Grow in={content.name ? true : false}>
        <Card className="card">
          <CardActionArea>
            <CardContent>
              <Typography gutterBottom variant="h5" component="h2">{content.name}</Typography>
              <Typography variant="body2" component="p">Birth year: {content.birth_year}</Typography>
              <Typography variant="body2" component="p">Gender: {content.gender}</Typography>
              <Typography variant="body2" component="p">Eye color: {content.eye_color}</Typography>
              <Typography variant="body2" component="p">Skin color: {content.skin_color}</Typography>
              <Typography variant="body2" component="p">Height: {content.height}</Typography>
              <Typography variant="body2" component="p">Mass: {content.mass}</Typography>
              <Typography variant="body2" component="p">Films: {content.films.join(', ')}</Typography>
            </CardContent>
          </CardActionArea>
          <CardActions>
            <Button
              color="primary"
              onClick={() => closeContentDetailsPanel()}
              variant="contained"
            >
              CLOSE
            </Button>
          </CardActions>
        </Card>
      </Grow>
    </Wrapper>
  );
}
