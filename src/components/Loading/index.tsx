import styled from 'styled-components';
/**
 * Easter egg loader :)
 */
const disableWindowScroll = (): void => {
  // Get the current page scroll position
  const scrollTop: number =
    (window.scrollY !== undefined ? window.scrollY : window.pageYOffset) ||
    document.documentElement.scrollTop;
  const scrollLeft: number =
    (window.scrollX !== undefined ? window.scrollX : window.pageXOffset) ||
    document.documentElement.scrollLeft;

  // If any scroll is attempted, set this to the previous value
  window.onscroll = (): void => {
    window.scrollTo(scrollLeft, scrollTop);
  };
};

const enableWindowScroll = (): void => {
  window.onscroll = (): void => {};
};

export const Loading = ({ showLoader }: any): any => {
  if (showLoader) {
    disableWindowScroll();

    return (
      <Loader>
        <div className="container">
          <div className="animation"/>
        </div>
      </Loader>
    );
  } else {
    enableWindowScroll();
    return null;
  }
};

const Loader = styled.div`
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  background-color: rgba(0, 0, 0, 0.7);
  z-index: 20;
  .container {
    position: absolute;
    top: 50%;
    left: 50%;
    width: 60px;
    height: 60px;
    transform: translate(-50%, -50%);
  }
  .animation {
    position: relative;
    width: 60px;
    height: 60px;
    background-color: #ffffff;
    border-radius: 100%;
    animation: pulseScaleOut 1s infinite ease-in-out;
  }
  @keyframes pulseScaleOut {
    0% {
      transform: scale(0);
    }
    100% {
      transform: scale(1);
      opacity: 0;
    }
  }
`;
