import styled from 'styled-components';

const Wrapper = styled.div`
  width: calc(100% - 100px);
  max-width: 400px;
  height: 100px;
  margin: 0 auto;
  padding-left: 50px;
  padding-right: 50px;
`;

export default Wrapper;
