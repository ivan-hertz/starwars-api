import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import { useSelector } from "react-redux";
import { selectSearchCount } from '../../store/search/search.selectors';
import Wrapper from './Wrapper';

interface ISearchBoxProps {
  executeSearch: (text: string) => void;
}

export function SearchBox(props: ISearchBoxProps) {
  const searchCount = useSelector(selectSearchCount);
  const executeSearch = props.executeSearch;
  const [keyword, setKeyword] = useState<string>('');
  const [message, setMessage] = useState<string>('');
  const [error, setError] = useState<boolean>(false); 

  const useStyles = (): any => {
    return makeStyles((theme) => ({
      root: {
        "& > *": {
          margin: theme.spacing(1),
          width: "25ch",
        },
      },
    }));
  }

  /**
   * This handler fills the store with the response in the background
   * @param event Input HTML Event
   */
  const handleSubmit = (event: React.FormEvent<HTMLFormElement>): void => {
    event?.preventDefault();
    if (keyword.length >= 3) {
      executeSearch(keyword);
    } else {
      setError(true);
      setMessage('Please type at least 3 char :)');
    }
  }

  /**
   * Thie handler intercepting the validation
   * @param event Input HTML Event
   */
  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>): void => {
    setKeyword(event?.target?.value);
    if(error !== false){
      setError(false);
      setMessage('');
    }
  }

  /**
   * Pure render, no mapping here.
   */
  return (
    <Wrapper>
      <form
        className={useStyles().root}
        noValidate
        autoComplete="off"
        onSubmit={(e) => handleSubmit(e)}
      >
        <TextField
          error={error}
          helperText={ !error && searchCount ? `Result count: ${searchCount}` : message }
          value={keyword}
          color="secondary"
          fullWidth={true}
          onChange={(e) => handleInputChange(e)}
          id="outlined-basic"
          label="Type a character name"
          variant="outlined"
        />
      </form>
    </Wrapper>
  );

}
