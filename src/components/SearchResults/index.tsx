import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { ContentService } from '../../services/ContentService';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { selectSearchCount, selectSearchResult } from '../../store/search/search.selectors';
import { IContent } from '../../common/interfaces';
import Wrapper from './Wrapper';

export const SearchResults = () => {
  const searchResults = useSelector(selectSearchResult);
  const searchCount = useSelector(selectSearchCount);
  const ListItemLink = (props: any) => (<ListItem button component="a" {...props} />);
  const getContent = (url: string) => ContentService.getPeople(url);
  
  useEffect(() => {
    // Debug purpose only
    console.info({ searchResults });
  }, [searchResults]);

  /**
   * Render helper function.
   */
  const renderResults = (content: IContent) => {
    return (
      <ListItemLink key={ContentService.safeId(content.name)} onClick={() => getContent(content.url)}>
        <ListItemText primary={content.name} secondary={`Birth year: ${content.birth_year}`} />
      </ListItemLink>
    );
  };
  /**
   * Feature request: No Result
   * Statement only truthy, if the state gets dirty 
   */
  const noResults = () => {
    const noResultStored = searchCount === 0;
    return noResultStored && <List>No results</List>;
  };

  return (
    <Wrapper>
      <div>
        <List component="nav" aria-label="main mailbox folders">
          {Object.keys(searchResults).map((key: any) => renderResults(searchResults[key]))}
          {noResults()}
        </List>
      </div>
    </Wrapper>
  );
}
