import styled from 'styled-components';
const Title = styled.div`
    margin: 10px;
    color: #fff;
    font-size: 0.7em;
    font-family: bold;
    text-transform: uppercase;
    font-family: Arial, Helvetica, sans-serif;
`;

export default Title;
