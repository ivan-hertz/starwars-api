import styled from 'styled-components';

const Wrapper = styled.div`
  .results {
    width: calc(100% - 40px);
    height: calc(100%);
    background-color: rgba(255, 255, 255, 0);
    overflow-y: scroll;
    padding: 20px;
    padding-top: 0;
    display: flex;
    justify-content: center;
    > div {
      border-left: 1px solid rgba(255, 255, 255, 0.1);
      border-right: 1px solid rgba(255, 255, 255, 0.1);
      max-width: 885px;
      width: 100%;
      height: 100%;
      margin-left: 20px;
      margin-right: 20px;
    }
    .action {
      margin: 10px !important;
    }
    .gridbox {
      padding: 20px;
      font-weight: bold;
      cursor: pointer;
      background-color: #bbb !important;
    }
  }
`;

export default Wrapper;
