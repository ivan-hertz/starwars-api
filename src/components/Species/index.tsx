import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import {
  selectSpeciesResult,
  selectSpeciesNext,
  selectSpeciesPrevious,
} from '../../store/species/species.selectors';
import { ISpecies } from '../../common/interfaces';
import { SpeciesService } from '../../services/SpeciesService';
import { Button } from '@material-ui/core';
import { ContentService } from '../../services/ContentService';
import Wrapper from './Wrapper';
import Title from './Title';

export function Species() {
  const speciesPrevious = useSelector(selectSpeciesPrevious);
  const speciesNext = useSelector(selectSpeciesNext);
  const species = useSelector(selectSpeciesResult);
  const getAllSpecies = (url: string) => SpeciesService.getAllSpecies(url);
  const getSpecies = (url: string) => SpeciesService.getSpecies(url);
  
  useEffect(() => {
    // Debug purpose only
    species?.length && console.info({ species });
  }, [species]);

  /**
   * Render helper function.
   */
  const renderSpecies = () => {
    // Traversing the search results
    return Object.keys(species).map((item: any, key: any) => {
      const content: ISpecies = species[item];
      return (
        <Grid
          key={ContentService.safeId(content.name)}
          item
          sm={6}
          xs={12}
          onClick={() => getSpecies(content.url)}
        >
          <Paper elevation={3} variant="outlined" className="gridbox">
            {content.name}
          </Paper>
        </Grid>
      );
    });
  };

  return (
    <Wrapper>
      <Title>Select Species</Title>
      <div className="results">
        <div>
          <Grid container spacing={2}>
            {species?.length && renderSpecies()}
          </Grid>
          {speciesPrevious && (
            <Button
              color="primary"
              className="action"
              onClick={() => getAllSpecies(speciesPrevious)}
              variant="contained"
            >
              Previous
            </Button>
          )}
          {speciesNext && (
            <Button
              color="primary"
              className="action"
              onClick={() => getAllSpecies(speciesNext)}
              variant="contained"
            >
              Next
            </Button>
          )}
        </div>
      </div>
    </Wrapper>
  );
}
