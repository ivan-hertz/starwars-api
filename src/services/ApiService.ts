import axios from 'axios';
import { AxiosResponse } from 'axios';
export const { REACT_APP_SERVICE_API_URL } = process.env;

export default class APIService {

    /**
     * Default Connection wrapper for the API Layer
     * By this technique, you can change the http driver with ease
     * @param route - Your API route point
     * @param params - optional - url parameters
     * @param data - optional - Post data
     */
    public static defaultConnection(route: string, params?: any, data?: any): Promise<AxiosResponse<any>> {

        // connection string determines how to use the route command
        const connectionString = /(http(s?)):\/\//i.test(route) ? route : `${REACT_APP_SERVICE_API_URL}/${route}`;
        return !data
            ? axios.get(connectionString, { params })
            // We are not going to pass API_KEY, 
            // since we don't need customer implementation 
            // on the client side.
            // { api_key: process.env.API_KEY }               
            : axios.post(connectionString, data);

    }
    /**
     * Populates a collection of requiest urls into result object
     * @param collection - collection of urls
     * @returns 
     */
    public static async populate(collection: Array<string>) {
        
        // Buffering Promise request collection
        const requestCollection = collection.map((url: string) => APIService.defaultConnection(url));
        
        // Resolving all requests
        const items = await Promise.all(requestCollection);

        // Data extraction with fallback
        return items.map((p: any) => p.data ?? p);

    }
}
