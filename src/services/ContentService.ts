/**
 * Extendable Application Content service
 */
import APIService from './ApiService';
import Store from '../store/store';
import { storeContent } from '../store/content/content.actions';
import { setLoading } from '../store/application/application.actions';

class ContentService {

    /**
     * Since the these microservices are integral part of the application,
     * it handles the loading states from here.
     *
     * This member method fetches the content, then moves into the store.
     * Closing the detail modal wipes the data from content store.
     * @param url - content url
     */
    public static getPeople(url: string): void {
        Store.dispatch(setLoading(true));
        try {
            APIService.defaultConnection(url).then(async (response) => {
                const films = (await APIService.populate(response?.data?.films))
                    .map((film: any) => film.title);
                const people = { ...response?.data, films };
                Store.dispatch(storeContent(people));
                Store.dispatch(setLoading(false));
            });
        } catch (error) {
            new Error(error);
            Store.dispatch(setLoading(false));
        }
    }

    public static safeId(str: string): string {
        return str.toString().replace(/\W/g, '_');
    }

}

export { ContentService };
