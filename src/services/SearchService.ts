/**
 * Extendable Application Search service
 */
import APIService from './ApiService';
import Store from '../store/store';
import { storeSearchResults } from '../store/search/search.actions';
import { setLoading } from '../store/application/application.actions';
class SearchService {

    /**
     * Client-side search micro-service.
     * It always stores the result in the redux store.
     * Sets the loader inplace.
     * @param keyword - search string
     */
    public static searchByKeyword(keyword: string): void {
        Store.dispatch(setLoading(true));
        APIService.defaultConnection(`people/?search=${keyword}`).then((response) => {
            const results = response?.data;
            if (results) {
                Store.dispatch(storeSearchResults(results));
                Store.dispatch(setLoading(false));
            } else {
                return Promise.reject(false);
            }
        });
    }

}

export { SearchService };
