/**
 * Extendable Application Content service
 */
import APIService from './ApiService';
import Store from '../store/store';
import { storeSpecies } from '../store/species/species.actions';
import { setLoading } from '../store/application/application.actions';
import { storeSearchResults } from '../store/search/search.actions';

class SpeciesService {

    /**
     * A Species resource is a type of person 
     * or character within the Star Wars Universe.
     */
    public static getAllSpecies(url?: string): void {
        Store.dispatch(setLoading(true));
        APIService.defaultConnection(url ?? 'species').then((response) => {
            Store.dispatch(storeSpecies(response.data));
            Store.dispatch(setLoading(false));
        }, error => {
            Store.dispatch(setLoading(false));
        });
    }
    /**
     * swapi-ts is failing to fetch species, 
     * since incorrectly parsing the api url.
     * i had to intercept and extract people, direcly.
     * @param url - species api url
     */
    public static getSpecies(url: string): void {
        Store.dispatch(setLoading(true));
        try {
            APIService.defaultConnection(url).then(async (response) => {
                const results = (await APIService.populate(response?.data?.people));
                Store.dispatch(storeSearchResults({
                    results,
                    count: results?.length,
                }));
                Store.dispatch(setLoading(false));
            })
        } catch (error) {
            new Error(error);
            Store.dispatch(setLoading(false));
        }
    }

}

export { SpeciesService };
