export const SET_LOADING: string = 'SET_LOADING';
export const SHOW_LOADING: string = 'SHOW_LOADING';

export const setLoading = (enable: boolean) => ({
    type: SET_LOADING,
    payload: enable
});

export const showLoading = () => ({
    type: SHOW_LOADING,
});