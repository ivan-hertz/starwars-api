import { SET_LOADING, SHOW_LOADING } from './application.actions';

export interface IApplication {
    loading: boolean;
}
const initialState: IApplication = {
    loading: false
};

export const applicationReducer = (state = initialState, action: any) => {
    switch (action.type) {
        case SET_LOADING:
            return { loading: action.payload };
        case SHOW_LOADING:
            return { loading: true };

        default:
            return state;
    }
};
