import { IContent } from '../../common/interfaces';
export const STORE_CONTENT: string = 'STORE_CONTENT';
export const CLEAR_CONTENT: string = 'CLEAR_CONTENT';

export const storeContent = (content: IContent) => ({
    type: STORE_CONTENT,
    payload: content
});
export const clearContent = () => ({
    type: CLEAR_CONTENT
});
