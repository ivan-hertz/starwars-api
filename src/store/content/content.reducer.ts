import { STORE_CONTENT, CLEAR_CONTENT } from './content.actions';
import { IContent } from '../../common/interfaces';

export const contentInitialState: IContent = {
    birth_year: '',
    created: '',
    edited: '',
    eye_color: '',
    films: [],
    gender: '',
    hair_color: '',
    height: null,
    homeworld: '',
    mass: null,
    name: '',
    skin_color: '',
    species: [],
    starships: [],
    url: '',
    vehicles: []
};

export const contentReducer = (state = contentInitialState, action: any) => {
    switch (action.type) {
        case STORE_CONTENT:
            return { ...action.payload };
        case CLEAR_CONTENT:
            return { ...contentInitialState };
        default:
            return state;
    }
};
