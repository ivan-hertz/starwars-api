import { IContent } from '../../common/interfaces';
import { IRootReducers } from '../store.reducer';
import { createSelector } from '@reduxjs/toolkit';

const selector = (state: IRootReducers) => state.content;

export const selectContent = createSelector([selector], (state: IContent) => state);