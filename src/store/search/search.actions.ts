import { IResponse } from '../../common/interfaces';
export const STORE_SEARCH_RESULTS: string = 'STORE_SEARCH_RESULTS';
export const CLEAR_SEARCH_RESULTS: string = 'CLEAR_SEARCH_RESULTS';

export const storeSearchResults = (results: IResponse) => ({
    type: STORE_SEARCH_RESULTS,
    payload: results
});
export const clearSearchResults = () => ({
    type: CLEAR_SEARCH_RESULTS,
});
