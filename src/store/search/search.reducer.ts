import { CLEAR_SEARCH_RESULTS, STORE_SEARCH_RESULTS } from './search.actions';
import { IResponse } from '../../common/interfaces';

const initialState: IResponse = {
    count: null,
    results: null,
    next: null,
    previous: null,
};

export const searchReducer = (state = initialState, action: any) => {
    switch (action.type) {
        case STORE_SEARCH_RESULTS:
            return { 
                count: action.payload.count,
                results: action.payload.results,
                next: action.payload.next,
                previous: action.payload.previous,
            };
        case CLEAR_SEARCH_RESULTS:
            return initialState;
        default:
            return state;
    }
};
