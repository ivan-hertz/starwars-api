import { IResponse } from '../../common/interfaces';
import { IRootReducers } from '../store.reducer';
import { createSelector } from '@reduxjs/toolkit';

const selector = (state: IRootReducers) => state.search;

export const selectSearchResult = createSelector([selector], (state: IResponse) => state.results);
export const selectSearchCount = createSelector([selector], (state: IResponse) => state.count);
