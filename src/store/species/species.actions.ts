import { ISpecies } from '../../common/interfaces';
export const STORE_SPECIES: string = 'STORE_SPECIES';
export const storeSpecies = (results: ISpecies[]) => ({
    type: STORE_SPECIES,
    payload: results
});
