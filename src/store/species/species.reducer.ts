import { STORE_SPECIES } from './species.actions';
import { IResponse } from '../../common/interfaces';

const initialState: IResponse = {
    count: null,
    results: {},
    next: null,
    previous: null,
};

export const speciesReducer = (state = initialState, action: any) => {
    switch (action.type) {
        case STORE_SPECIES:
            return { ...action.payload };
        default:
            return state;
    }
};
