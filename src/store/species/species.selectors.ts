import { IResponse } from '../../common/interfaces';
import { IRootReducers } from '../store.reducer';
import { createSelector } from '@reduxjs/toolkit';

const selector = (state: IRootReducers) => state.species;

export const selectSpeciesResult = createSelector([selector], (state: IResponse) => state.results);
export const selectSpeciesCount = createSelector([selector], (state: IResponse) => state.results.length);
export const selectSpeciesNext = createSelector([selector], (state: IResponse) => state.next);
export const selectSpeciesPrevious = createSelector([selector], (state: IResponse) => state.previous);
