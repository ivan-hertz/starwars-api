import { combineReducers } from 'redux';
import { searchReducer } from './search/search.reducer';
import { speciesReducer } from './species/species.reducer';
import { IResponse, IContent } from '../common/interfaces';
import { contentReducer } from './content/content.reducer';
import { applicationReducer, IApplication } from './application/application.reducer';

export interface IRootReducers {
    search:IResponse,
    species: IResponse,
    content: IContent,
    application: IApplication,
}
export const rootReducers = {
    search: searchReducer,
    species: speciesReducer,
    content: contentReducer,
    application: applicationReducer,
}

export default combineReducers(rootReducers);
